
void current_annealing()
{
	//gErrorIgnoreLevel = 6001;

	TString fname = "../gold_data/GIFpp_Data_annealing.root";

	TFile *file = TFile::Open(fname);
	TTree *tree;

	file->GetObject("GIFpp_DATA_MIPs", tree);
	
	//TH1F *htemp = new TH1F("htemp","htemp",1000,0,2e6);	
	//tree->Draw("current1:time_stamp","BV1==39.5&&dip_15402>0.0062&&dip_filter>=2.2","hist");
	//htemp->Draw();

	int ch=0, ch0 = 0, k = 0;
	float I1, I2, G1, G2, eI1, eI2, eG1, eG2, BV1, BV2, error;
	long int time_stamp, time_stamp0, time_stamp_prev;
	std::string *runtype = 0;
	std::vector<float> I1v, I2v, G1v, G2v, I1va, I2va, G1va, G2va, I1vad, tsvad;
	std::vector<long int> tsv, tsva;

	//float I01 = 51, I02 = 51;
	float Im1 = 57.80056134, Im2 = 61.67143593;

	//float G01 = 409, G02 = 384;
	int T0 = 1762260;

	int nentries = tree->GetEntries();
	tree->SetBranchAddress("MIP1", &I1);
	tree->SetBranchAddress("MIP2", &I2);
	tree->SetBranchAddress("eMIP1", &eI1);
	tree->SetBranchAddress("eMIP2", &eI2);
	tree->SetBranchAddress("Gain1", &G1);
	tree->SetBranchAddress("Gain2", &G2);
	tree->SetBranchAddress("eGain1", &eG1);
	tree->SetBranchAddress("eGain2", &eG2);
	tree->SetBranchAddress("BV1", &BV1);
	tree->SetBranchAddress("BV2", &BV2);
	tree->SetBranchAddress("time_stamp", &time_stamp);
	tree->SetBranchAddress("runtype", &runtype);

	//tree->GetEntry(0);

	time_stamp_prev = 0;

	TGraphErrors *gr = new TGraphErrors();

	for (int i = 0; i < nentries; ++i) {
		tree->GetEntry(i);

		//std::cout << *runtype << std::endl;

		if ((BV1==40)&&(BV2==40)&&(*runtype=="SOURCE")&&(time_stamp>=T0)) {

			I1v.push_back(I1);//I01-0.897*TMath::Exp(-dose/1640));
			I2v.push_back(I2);///I02/G2*G02);
			//G1v.push_back(G1/G01);
			//G2v.push_back(G2/G02);
			tsv.push_back((time_stamp-T0)/3600);
		
			if (I2>36000) {
				error = TMath::Sqrt(TMath::Power(eI2/G2, 2)+TMath::Power(I2*eG2/G2/G2, 2));
				gr->SetPoint(k, (time_stamp-T0)/3600, (I2/G2/TMath::Sqrt(2)/Im2));
				gr->SetPointError(k, 0.0, error/TMath::Sqrt(2)/Im2);
				k++;
			}

			/*if (I1>35000) {
				error = TMath::Sqrt(TMath::Power(eI1/G1, 2)+TMath::Power(I1*eG1/G1/G1, 2));
				gr->SetPoint(k, (time_stamp-T0)/3600, (I1/G1/TMath::Sqrt(2)/Im1));
				gr->SetPointError(k, 0.0, error/TMath::Sqrt(2)/Im1);
				k++;
			}*/

			//if (I1>36000) gr->AddPoint((time_stamp-T0)/3600, I2);///G2/TMath::Sqrt(2));///I02-1);
	
			//if ((dose<60)||(dose>68))
			//if ((dose<50)||(dose>85))
			//	if ((dose<105)||(dose>130))
			//		if ((dose<140))
			//if (time_stamp/3600<140)
						//gr->AddPoint(dose, I1/I01-0.897*TMath::Exp(-dose/1640));
						//gr->AddPoint(time_stamp/3600, I1/I01-0.897*TMath::Exp(-dose/1640));
			//std::cout << I1 << std::endl;
		}
	}

	/*int Na = 2;
	int Nd = 2;
	int Na2 = 4;

	for (uint i = 0; i < I1v.size()-Na+1; ++i) {
		
		auto it1 = I1v.cbegin() + i;
		auto it2 = I1v.cbegin() + i + Na;
		auto it3 = tsv.cbegin() + i;
		auto it4 = tsv.cbegin() + i + Na;
		auto it5 = Dose.cbegin() + i;
		auto it6 = Dose.cbegin() + i + Na;
		auto it7 = I2v.cbegin() + i;
		auto it8 = I2v.cbegin() + i + Na;
		//auto it9 = G1v.cbegin() + i;
		//auto it10 = G1v.cbegin() + i + Na;
		//auto it11 = G2v.cbegin() + i;
		//sauto it12 = G2v.cbegin() + i + Na;
		I1 = TMath::Mean(it1, it2);
		I2 = TMath::Mean(it7, it8);
		//G1 = TMath::Mean(it9, it10);
		//G2 = TMath::Mean(it11, it12);
		time_stamp = TMath::Mean(it3, it4);
		dose = TMath::Mean(it5, it6);
		I1va.push_back(I1);
		I2va.push_back(I2);
		//G1va.push_back(G1);
		//G2va.push_back(G2);
		tsva.push_back(time_stamp);
		Dosea.push_back(dose);
		//gr->AddPoint(time_stamp, I1);
		//gr->AddPoint(dose, I1);
		//std::cout << time_stamp << "\t" << I1 << std::endl;

	}

	for (uint i = 0; i < I1v.size()-Na-Nd; ++i) {
		
		I1 = (I1va[i+Nd]-I1va[i])/(tsva[i+Nd]-tsva[i])/I1va[i+Nd];
		I2 = (I2va[i+Nd]-I2va[i])/(tsva[i+Nd]-tsva[i]);
		time_stamp = tsva[i] + (tsva[i+Nd]-tsva[i])/2;
		dose = Dosea[i] + (Dosea[i+Nd]-Dosea[i])/2;
		//gr->AddPoint(time_stamp, I1);
		I1vad.push_back(I1);
		tsvad.push_back(time_stamp);
		//if ((dose>12)&&(dose<17)) continue;
		//if ((dose>50)&&(dose<85)) continue;
		//if ((dose>100)&&(dose<126)) continue;
		//if (dose>133) continue;
		//gr->AddPoint(dose, I1);
		//std::cout << time_stamp << "\t" << I1 << std::endl;

	}

	
	for (uint i = 0; i < I1v.size()-Na-Nd-Na2; ++i) {
		
		auto it1 = I1vad.cbegin() + i;
		auto it2 = I1vad.cbegin() + i + Na2;
		auto it3 = tsvad.cbegin() + i;
		auto it4 = tsvad.cbegin() + i + Na2;
		I1 = TMath::Mean(it1, it2);
		time_stamp = TMath::Mean(it3, it4);
		gr->AddPoint(time_stamp, I1);

	}*/

	TCanvas *c1 = new TCanvas("new","new",1200,600);
	//c1->Divide(1,2);

	TF1 *f = new TF1("exp","[0]*exp(-[3]*exp(-x/[2]))+(1-[0])*exp(-[4]*exp(-x/[1]))", 0, 700);
	//TF1 *f = new TF1("exp","[0]*exp(-x/[1])+[3]*exp(-x/[2])", 0, 14);
	//TF1 *f = new TF1("exp","[0]*exp(-x/[2])", 120, 144);
	//f->SetParameter(4,1);
	f->SetParameter(0, 0.784835);
	f->SetParameter(1, 1360.74);	
	f->SetParameter(2, 59.9916);
	f->SetParameter(3, 0.0288621);
	f->SetParameter(4, 1.18649);
	//c1->cd(1);
	gr->Fit(f,"R");
	
	gr->Draw("");

	//c1->cd(2);

	//float p0, p1, p2;
	//TF1 *f = new TF1("exp","[0]+[1]*exp([2]*x)");

	///

	delete runtype;

}
