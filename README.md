Root scripts to visualize and analyze packed in TTree data obtained from GIFpp Cold Run setup.


In data_sim scripts, we are considering that there are two independent differential equations for fast and slow radicals (defects) with the following parameters:

alpha - dose-rate constant, determines the how fast radicals grows with current dose-rate

Rfs - ratio between fast and slow components (relative efficiency) for radicals

Tf and Ts - fast and slow components of annealing

Dfs - ratio between how fast and slow components reducing photocurrent


data_sim_compare - to plot and compare different version of annealing parts together

data_sim6 - only quadratic part

data_sim5 - linear and quadratic annealing parts in equation

data_sim4 - just linear part

data_sim1,2,3 - just previous implementations
